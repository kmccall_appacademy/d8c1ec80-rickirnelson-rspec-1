def translate(str)
  str.split(' ').map {|word| pig(word)}.join(' ')
end

def pig(str)
  return str + "ay" if str[0] =~ /[aeiou]/
  v_idx = str.chars.select.with_index {|s,i| i if "aeiou".chars.include?(s)}
  if str.chars.each_cons(2).map(&:join).include?("qu")
    return str[str.index(v_idx[1])..-1] + str[0...str.index(v_idx[1])] + "ay"
  end
  return str[str.index(v_idx[0])..-1] + str[0...str.index(v_idx[0])] + "ay"
end
