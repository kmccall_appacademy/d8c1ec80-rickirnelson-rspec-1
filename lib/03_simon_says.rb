def echo(str)
  str
end

def shout(str)
  str.upcase
end

def repeat(str, times=2)
  ((str + " ")*times).strip
end

def start_of_word(str,n)
  str[0...n]
end

def first_word(str)
  str.split(' ')[0]
end

def titleize(str)
  sen = str.split(' ').map do |word|
    word.length >= 4 && word != "over" ? word.capitalize : word
  end.join(' ')
  sen[0] = sen[0].upcase
  sen
end
