def add(x,y)
  x+y
end

def subtract(x,y)
  x-y
end

def sum(arr)
  arr.empty? ? 0 : arr.reduce(:+)
end

def multiply(arr)
  arr.reduce(:*)
end

def power(x,y)
  x**y
end

def factorial(n)
  if n == 0
    return 1
  end
  n * factorial(n-1)
end
